package lezione2;

import java.util.ArrayList;
import java.util.List;

public class Gruppo {
	
	private List <Persona> people;

	Gruppo (){
		this.people = new ArrayList<Persona>();
	}
	
	//ritorna true se l'operazione e' andata a buon fine, false altrimenti
	public boolean addPerson(Persona person) {
		return people.add(person);
	}
	
	public boolean deletePerson(Persona person) {
		return people.remove(person);
	}
	
	public Persona getPerson(String name) {
		for (int index = 0; index < people.size(); index++){
			if (people.get(index).getName().equalsIgnoreCase(name)) {
				return people.get(index);
			}
		}
		//in caso name non sia presente nel database
		return null;
	}

}
