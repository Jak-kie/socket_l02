package lezione2;

import java.util.ArrayList;
import java.util.List;

public class Persona {
	private String name;
	private List <String> friends;					//gli amici di "name"
	
	Persona (String name){
		this.name = name;
		this.friends = new ArrayList<String>();
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
		
	//possiamo aggiungere piu amici
	public void addFriend(String arrayFriend[]) {
		for(int index = 0; arrayFriend.length > index; index++) {
			friends.add(arrayFriend[index]);
		}
	}
	
	//possiamo cancellare piu amici
	public void deleteFriend(String arrayFriend[]) {
		for(int index = 0; arrayFriend.length > index; index++) {
			friends.remove(arrayFriend[index]);
		}
	}
	
	public String getFriendList() {
		if (friends.isEmpty())
			return name + " non ha amici";
		return "Amici di " + name + ": " + friends.toString();
	}
	
	public boolean isFriend (String friendName) {
		//ritorna true se friendName e' amico di Name
		if (friends.contains((Object)friendName)) return true;
		//altrimenti ritorna False
		return false;
	}

}
