package lezione2;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {
	public static void main(String[] args) {
		Socket socket; // socket
		InetAddress serverAddress; // server address
		int serverPort; // server port
		BufferedReader in; // source stream of bytes
		PrintWriter out;
		String messageString = ""; // text to be displayed
				
		try {
			serverAddress = InetAddress.getByName("localhost");
			serverPort = 53000;
			socket = new Socket(serverAddress, serverPort);
			System.out.println("Connected to: " + socket.getInetAddress());
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream(), true);
			
			Scanner inputUserChannel = new Scanner (System.in);

			while(true) {
				System.out.println("Digita un comando, oppure 'exit' per uscire");
				String inputUserText = inputUserChannel.next();
				out.println(inputUserText);

				//prima si chiede al server che vuoi chiudere la connessione, cosi che la chiuda sia lui che te
				if(inputUserText.equalsIgnoreCase("exit"))
					break;
				
				//ricordarsi di sostituire in maniera piu efficiente messageString
				messageString = in.readLine();
				System.out.println(messageString);
			}
			
			System.out.println("Connessione chiusa col server");
			inputUserChannel.close();
			socket.close();
		}catch(IOException e) {
			e.getStackTrace();
		}
	}
}
