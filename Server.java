package lezione2;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Server {
	
	final static Gruppo people = new Gruppo();
	static String messageString = "";
	static String[] arrayNames = null;
	
	public static void main(String[] args) {
		//inizializeData();
		BufferedReader in;
		PrintWriter out;	// source destination of bytes
		
		try {
			ServerSocket listenSocket = new ServerSocket(53000);
			System.out.println("Running Server: " + "Host=" + listenSocket.getInetAddress() + " Port=" + listenSocket.getLocalPort());
			Socket clientSocket = null;
			
			//per effettuare connessioni con nuovi socket/client
			while(true) {
				clientSocket = listenSocket.accept();
				in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				out = new PrintWriter(clientSocket.getOutputStream(), true);
				System.out.println("Connessione effettuata col client " + clientSocket.getInetAddress());
				
				//per effettuare una comunicazione continuativa con un socket specifico, finche non chiama "exit"
				while(true) {
					//legge il messaggio ricevuto
					messageString = in.readLine();

					//se il client ha chiesto di chiudere la connessione, si chiude anche qua
					//devi permettere comunque nuove richieste di pervenire
					if(messageString.equalsIgnoreCase("exit"))
						break;

					//controllo del parsing affidato ad una funzione esterna
					messageString = parsingInput (messageString);
					
					//invia il messaggio al client
					out.println(messageString);
				}
				clientSocket.close();
				System.out.println("Connessione chiusa col client");
			}
			//listenSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//debug per verificare la funzionalita' di Persona.Java e Gruppo.Java
	public static void inizializeData() {
		people.addPerson(new Persona ("Aldo"));
		people.addPerson(new Persona ("Giovanni"));
		people.addPerson(new Persona ("Giacomo"));
		
		people.getPerson("Aldo").addFriend(new String[] {people.getPerson("Giovanni").getName(), people.getPerson("Giacomo").getName()});
		System.out.println(people.getPerson("Aldo").getName());
		System.out.println(people.getPerson("Aldo").getFriendList());
		System.out.println(people.getPerson("Aldo").isFriend("Giovanni"));
		people.getPerson("Aldo").setName("Claudia");
		people.getPerson("Claudia").deleteFriend(new String[] {people.getPerson("Giovanni").getName()});
		System.out.println(people.getPerson("Claudia").getFriendList());
		System.out.println(people.getPerson("Claudia").isFriend("Giovanni"));
		
	}
	
	public static String parsingInput (String input) {
		//non capisco perche non vada, anche perche' su https://regexr.com/5on1o il test lo passa, e il regex e' identico
		//CharSequence tmpInput = input;					//alternativa a usare input, anche se non cambia nulla
		
		final String regex = "[+-]?[a-zA-Z]+[|[a-zA-Z]+[;[a-zA-Z]+]*]?";
		System.out.println("Il comando ricevuto e' accettabile: " + Pattern.matches(regex, input));

		Matcher arithmeticExpression = Pattern.compile("[+-]+").matcher(input);
		Matcher name = Pattern.compile("[a-zA-Z]+").matcher(input);
		
		//creiamo le liste che terranno i nostri elementi del messaggio
		List<String> expressionList = new ArrayList<String>(); 
		List<String> nameList = new ArrayList<String>();
		
		while(name.find()) {
			nameList.add(name.group());
		}
		while(arithmeticExpression.find()) {
			expressionList.add(arithmeticExpression.group());
		}
		
		//se non abbiamo simboli, expressionList.isEmpty() == true
		
		//salviamo in un array i names, tranne il primo
		//dimensione -1 perche il primo non va messo dentro l'array
		arrayNames = new String[nameList.size()-1];
		for(int index = 1; index < nameList.size(); index++) {
			arrayNames[index-1] = nameList.get(index);
		}
		
		//utilizzato per semplificare il codice
		Persona tmp_person = people.getPerson(nameList.get(0));
		
		if(expressionList.isEmpty()) {
			if(arrayNames.length == 0) {
				//esempio: marco
				//se non e' stato mai inserito nameList.get(0)
				if(tmp_person == null)
					return "Nome non nel database";
				return tmp_person.getFriendList();
			}
			else {
				if(tmp_person == null)
					return "Nome non nel database";
				if (tmp_person.isFriend(nameList.get(1))) {
					//esempio: Marco|Alberto
					//messageString = "true";
					return "true";
				}
				else {
					//messageString = "false";
					return "false";
				}
			}
		}
		else{
			if(arrayNames.length == 0) {
				if(expressionList.get(0).equalsIgnoreCase("+")) {
					//AGGIUNGI AL DATABASE, esempio +Marco
					if(people.addPerson(new Persona (nameList.get(0))))
						return "Utente '" + nameList.get(0) + "' e' stato registrato";
				}
				else {
					//RIMUOVI DAL DATABASE, esempio -Marco
					if(tmp_person == null)
						return "Nome non nel database";
					if(people.deletePerson(tmp_person))
						return "Utente '" + nameList.get(0) + "' e' stato eliminato";
				}
			}
			else {
				if (expressionList.get(0).equalsIgnoreCase("+")) {
					if(tmp_person == null)
						return "Nome non nel database, impossibile aggiungere amici";
					tmp_person.addFriend(arrayNames);
					return "Amico/amici registrati correttamente";
				}
				//caso in cui e' stato chiesto "-"
				else {
					if(tmp_person == null)
						return "Nome non nel database, impossibile eliminare amici";
					tmp_person.deleteFriend(arrayNames);
					return "Amico/amici eliminati correttamente";
				}
			}
		}
		
		//questo return e' inutile, ma se non lo faccio java non vede che ci sta un return di una string
		//a sto punto sfruttiamolo per indicare che non c'e' alcun risultato valido ritornabile
		return "ERRORE";
	}
}
